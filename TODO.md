# TODO
## Planned

Features I'm planning to add in the near future, sorted by importancy:

 4. **Object/World space simulation**
 5. **Custom Editor** - Play the particle effect in edit mode.
 6. **Trail particles**
 7. **Shader API** - more intuitive shader programming. Maybe by overriding functions like 
`float3 SetParticlePositionOS(ParticleData particle)`, 
`LifetimeData SetParticleLifetime(ParticleData particle)` etc.
 8. **Shader templates**

## Backlog

Some Ideas:

 - **Subpixel Particles** - for transparent particles. Kind of shader built in antialiasing and support for particles smaller than screen pixels.
 - **Volume based particle change** - place 3D volumes in the scene, to modify particle behaviour using these volumes.
 - **Lighting and receiving shadows**
 - **Casting shadows**
 - **Showcase project**
 - **VR Compatibility**
 - **Immediate draw** - single call in Update method of any component to draw a particle instance on next frame render.

## Done!

 1. **LOD** - generating different amount of particles based on the length to the camera.
 3. **Component free particle instancing** - posibility to start any particle effect without creating MonoBehavior and GameObject, just by giving basic particle information (material, position, particle count etc.)
