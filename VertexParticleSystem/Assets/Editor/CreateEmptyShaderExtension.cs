﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor
{
	public static class CreateEmptyShaderExtension
	{
		public static void CreateShader(string hiddenShaderName, string createdShaderName, string newFileName)
		{
			var selected = Selection.activeObject;
			if (selected != null)
			{
				string pathToCopyInto = AssetDatabase.GetAssetPath(selected);
				if (!(selected is DefaultAsset))
				{
					pathToCopyInto = pathToCopyInto.Substring(0, pathToCopyInto.LastIndexOf('/'));
				}

				var shader = Shader.Find(hiddenShaderName);
				string newShaderPath = pathToCopyInto + "/" + newFileName;
				File.Copy(AssetDatabase.GetAssetPath(shader), newShaderPath);
				string[] shaderText = File.ReadAllLines(newShaderPath);
				for (int i = 0; i < shaderText.Length; i++)
				{
					var line = shaderText[i];
					if (line.Length > 6)
					{
						if (line.Substring(0, 6).ToLower() == "shader")
						{
							line = "Shader \"" + createdShaderName + "\"";
							shaderText[i] = line;
						}
					}
				}
				File.WriteAllLines(newShaderPath, shaderText);
				AssetDatabase.Refresh();
			}
		}

		[MenuItem("Assets/Create/Shader/Basic fragment and vertex", priority = int.MinValue)]
		private static void CreateBasicFragmentVertex() =>
			CreateEmptyShaderEditor.CreateShader(
				hiddenShaderName: "Hidden/BasicFragmentVertexTemplate",
				createdShaderName: "Templates/FragmentAndVertexTemplate",
				newFileName: "BasicFragmentVertex.shader"
			);

		[MenuItem("Assets/Create/Shader/Empty fragment and vertex", priority = int.MinValue)]
		private static void CreateParticleShaderTemplate() =>
			CreateEmptyShaderEditor.CreateShader(
				hiddenShaderName: "Hidden/EmptyFragmentAndVertex",
				createdShaderName: "Templates/EmptyFragmentAndVertex",
				newFileName: "EmptyFragmentVertex.shader"
			);


		[MenuItem("Assets/Create/Shader/Hlsl include", priority = int.MinValue + 1)]
		private static void CreateEmptyHLSLInclude()
		{
			var selected = Selection.activeObject;
		
			if (selected != null)
			{
				string pathToCopyInto = AssetDatabase.GetAssetPath(selected);
				if (!(selected is DefaultAsset))
				{
					pathToCopyInto = pathToCopyInto.Substring(0, pathToCopyInto.LastIndexOf('/'));
				}

				CreateEmptyHLSLEditor.ShowWindow(pathToCopyInto);
			}
		}
	}

	public class CreateEmptyHLSLEditor : EditorWindow
	{
		public static string newHlslIncludePath = "";
		public static string fileName = "";
		public static string defineName = "";

		public static void ShowWindow(string path)
		{
			newHlslIncludePath = path;
			ResetFields();
			var window = GetWindow<CreateEmptyHLSLEditor>(true, "New Hlsl Include", true);
			window.maxSize = new Vector2(400, 70);
			window.minSize = window.maxSize;
		}

		public static void ResetFields()
		{
			fileName = "newInclude.hlsl";
			defineName = "NEW_INCLUDE_DEFINED";
		}

		void OnGUI()
		{
			fileName = EditorGUILayout.TextField("Include file name", fileName);
			defineName = EditorGUILayout.TextField("Include guard name", defineName);
			if(GUILayout.Button("Create"))
			{
				StringBuilder sb = new StringBuilder();
				sb.AppendLine($"#ifndef {defineName}");
				sb.AppendLine($"#define {defineName}");
				for (int i = 0; i < 3; i++) sb.AppendLine();
				sb.AppendLine("#endif");
				File.WriteAllText($"{newHlslIncludePath}/{fileName}", sb.ToString());
				AssetDatabase.Refresh();
				Close();
			}
		}
	}

	public class CreateEmptyShaderEditor : EditorWindow
	{
		public static string sourceShaderName = "";
		public static string shaderName = "";
		public static string fileName = "";

		public static void CreateShader(string hiddenShaderName, string createdShaderName, string newFileName)
		{
			sourceShaderName = hiddenShaderName;
			shaderName = createdShaderName;
			fileName = newFileName;
			ShowWindow();
		}

		public static void ShowWindow()
		{
			ResetFields();
			var window = GetWindow<CreateEmptyShaderEditor>(true, "New Hlsl Include", true);
			window.maxSize = new Vector2(400, 70);
			window.minSize = window.maxSize;
		}

		public static void ResetFields()
		{
			fileName = "newShader.shader";
		}

		void OnGUI()
		{
			fileName = EditorGUILayout.TextField("Created file name", fileName);
			shaderName = EditorGUILayout.TextField("Shader name", shaderName);
			if (GUILayout.Button("Create"))
			{
				CreateEmptyShaderExtension.CreateShader(sourceShaderName, shaderName, fileName);
				Close();
			}
		}
	}
}
