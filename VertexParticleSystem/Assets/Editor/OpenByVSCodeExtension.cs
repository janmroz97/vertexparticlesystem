﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class OpenByVSCodeExtension
{
	private static string vsCodePath = "Code.exe";

	[MenuItem("Assets/Open by VSCode")]
	private static void OpenByVSCode()
	{
		var selected = Selection.activeObject;
		if (selected != null)
		{
			string shaderPath = Application.dataPath + "/" + AssetDatabase.GetAssetPath(selected).Substring(7); 
			var process = new System.Diagnostics.Process();
			process.StartInfo.FileName = vsCodePath;
			process.StartInfo.Arguments = "\"" + shaderPath + "\"";
			process.Start();
		}
	}
	
	[MenuItem("Assets/Open by VSCode", true)]
	private static bool OpenByVSCodeValidation()
	{
		return Selection.activeObject is Shader || 
			Selection.activeObject is MonoScript || 
			Selection.activeObject is TextAsset;
	}
}
