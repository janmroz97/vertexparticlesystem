Shader "Test/Unlit"
{
    Properties
    {
        _BaseColor("Color", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        Blend One One
        Cull Off

        ZWrite Off

        Pass
        {
            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            #define TIME (_ParticleTime * 0.05)

            struct VertexData
            {
                float4 positionOS : POSITION;
                int particleID : TEXCOORD0;
            };

            struct FragmentData
            {
                float4 clipPos : SV_POSITION;
                float linearID : TEXCOORD0;
                float3 positionWS : TEXCOORD1;
            };

            CBUFFER_START(UnityPerMaterial)
                float4 _BaseColor;
                int _StartParticleIndex;
                int _ParticlesCount;
                float _ParticleTime;
            CBUFFER_END


            float hash11(float x)
            {
                return frac(sin(x * 4752.541) * 3432.8964);
            }

            float3 hash31(float x)
            {
                return frac(sin(x * float3(342.5763, 865.98633, 763.78537)) * 3754.7852);
            }

            FragmentData vert(VertexData input)
            {
                FragmentData output = (FragmentData)0;
                input.particleID += _StartParticleIndex;
                float linearID = (float)input.particleID / (float)_ParticlesCount;
                input.positionOS *= 0.01;
                float3 particlePositionOS = float3(sin(TIME * 30.0 + linearID * 60.28), 
                                               cos(TIME * 21.0 + linearID * 60.28),
                                               -sin(TIME * 16.0 + linearID * 60.28));

                particlePositionOS.x += sin(TIME + particlePositionOS.y * 2.1) * 0.4;

                particlePositionOS += (hash31(linearID * 10.0) - 0.5) * float3(sin(TIME + particlePositionOS.x * 3.0), 0.2, 0.3);

                float3 particlePositionWS = TransformObjectToWorld(particlePositionOS);
                output.positionWS = particlePositionWS;

                float3 particlePositionVS = TransformWorldToView(particlePositionWS);
                float3 positionVS = particlePositionVS + input.positionOS.xyz;
                output.clipPos = mul(UNITY_MATRIX_P, float4(positionVS, 1.0));
                output.linearID = linearID;

                return output;
            }

            half4 frag(FragmentData input) : SV_Target
            {
                return 0.1;
            }
            ENDHLSL
        }
    }
    FallBack Off
}
