Shader "VertexParticles/BasicParticles"
{
    Properties {}

    SubShader
    {
        Tags {}

        
        Pass
        {
            Cull Off
            ZTest Always
            ZWrite Off

            Blend One Zero


            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            struct vertexData
            {
                float3 positionOS : POSITION;
                int particleID : TEXCOORD0;
            };

            CBUFFER_START(UnityPerMaterial)
                float _Seed;
                int _ParticlesCount;
                int _StartParticleIndex;
            CBUFFER_END

            struct fragmentData
            {
                float4 clipPos : SV_POSITION;
                int particleID : TEXCOORD0;
                float linearParticleID : TEXCOORD1;
            };

            float3 hash33(float3 x)
            {
                return frac(sin(mul(x, float3x3(12.486, 43.765437, 78.78546, 7.74532, 76.476532, 12.8754, 38.478515, 9.89641, 23.48632))) * 242.43782);
            }

            fragmentData vert(vertexData input)
            {
                fragmentData output;
                int particleID = 0;//input.particleID + _StartParticleIndex;
                float linearParticleID = 0.0;//(float)particleID / (float)_ParticlesCount;
                output.particleID = particleID;
                output.linearParticleID = linearParticleID;
                output.clipPos = TransformObjectToHClip(input.positionOS.xyz);
                return output;
            }

            float4 frag(fragmentData input) : SV_Target
            { 
				discard;
                //just some animated color
                return sin(_Time.z + float4(0.0, 2.0, 4.0, 0.0)) * 0.5 + 0.5;
            }

            ENDHLSL
        }
    }
}
