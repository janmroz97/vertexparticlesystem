Shader "Hidden/Vertex Particle System/ErrorFallback"
{
    Properties {}

    SubShader
    {
        Tags {}

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            struct vertexData
            {
                float4 positionOS : POSITION;
            };

            struct fragmentData
            {
                float4 clipPos : SV_POSITION;
            };

            fragmentData vert(vertexData input)
            {
                fragmentData output;
                output.clipPos = TransformObjectToHClip(input.positionOS.xyz);
                return output;
            }

            float4 frag(fragmentData input) : SV_Target
            { 
                //just some animated color
                return sin(_Time.z + float4(0.0, 2.0, 4.0, 0.0)) * 0.5 + 0.5;
            }

            ENDHLSL
        }
    }
}
