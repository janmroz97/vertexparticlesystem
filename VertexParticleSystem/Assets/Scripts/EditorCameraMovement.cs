﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorCameraMovement : MonoBehaviour
{
	[SerializeField]
	private float cameraSpeed = 4.0f;

	[SerializeField]
	private Vector2 rotationSpeed = new Vector2(-7.0f, 7.0f);

	private Vector2 rotationAngles = Vector2.zero;

	void Update()
    {
		if (Input.GetKey(KeyCode.Mouse1))
		{
			// --------------- MOVEMENT ------------------------
			Vector3 movementVec = Vector3.zero;
			if (Input.GetKey(KeyCode.W)) movementVec += Vector3.forward;
			if (Input.GetKey(KeyCode.S)) movementVec += Vector3.back;
			if (Input.GetKey(KeyCode.A)) movementVec += Vector3.left;
			if (Input.GetKey(KeyCode.D)) movementVec += Vector3.right;
			if (Input.GetKey(KeyCode.Q)) movementVec += Vector3.down;
			if (Input.GetKey(KeyCode.E)) movementVec += Vector3.up;

			movementVec *= Time.deltaTime;
			movementVec *= cameraSpeed;
			if (Input.GetKey(KeyCode.LeftShift)) movementVec *= cameraSpeed;

			Vector3 pos = transform.position;
			transform.Translate(movementVec, Space.Self);


			// ------------------ ROTATION ------------------------
			rotationAngles.y += Input.GetAxis("Mouse X") * rotationSpeed.y;
			rotationAngles.x += Input.GetAxis("Mouse Y") * rotationSpeed.x;
			rotationAngles.x = Mathf.Clamp(rotationAngles.x, -90.0f, 90.0f);

			transform.rotation = Quaternion.Euler(rotationAngles.x, rotationAngles.y, 0.0f);
		}
    }
}
