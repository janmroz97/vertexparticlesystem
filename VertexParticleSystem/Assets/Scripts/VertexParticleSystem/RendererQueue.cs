﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.VertexParticleSystem
{
	public class VertexParticleQueue : IEnumerable<VertexParticleInstance>
	{
		private struct SortableRenderer
		{
			public VertexParticleInstance renderer;
			public float dist;
			public int queue;

			public SortableRenderer(VertexParticleInstance renderer, Vector3 sortPoint)
			{
				this.renderer = renderer;
				Vector3 distVec = (Vector3)renderer.LocalToWorldMatrix.GetColumn(3) - sortPoint;
				dist = Mathf.Abs(distVec.x) + Mathf.Abs(distVec.y) + Mathf.Abs(distVec.z);
				queue = renderer.Material.renderQueue;
			}
		}

		public enum SortOrder { Opaque, Transparent }

		public SortOrder sortOrder = SortOrder.Transparent;

		public List<VertexParticleInstance> ActiveRenderers { get; private set; } = new List<VertexParticleInstance>();
		private List<VertexParticleInstance> renderersToAdd = new List<VertexParticleInstance>();
		private List<VertexParticleInstance> renderersToRemove = new List<VertexParticleInstance>();

		public VertexParticleQueue(SortOrder sortOrder) => this.sortOrder = sortOrder;

		public bool HasRenderers => ActiveRenderers.Count > 0;

		//Command pattern. Enqueue commands, and then execute. Implemented as is to not modify active renderers while iterating.
		public void EnqueueAddCommand(VertexParticleInstance renderer) => renderersToAdd.Add(renderer);

		public void EnqueueRemoveCommand(VertexParticleInstance renderer) => renderersToRemove.Add(renderer);

		public void ExecuteCommands()
		{
			foreach (var renderer in renderersToRemove) ActiveRenderers.Remove(renderer);
			renderersToRemove.Clear();

			foreach (var renderer in renderersToAdd) ActiveRenderers.Add(renderer);
			renderersToAdd.Clear();
		}

		//distances to camera are firstly baked into this list, then list is sorted.
		private List<SortableRenderer> sortableRenderers = new List<SortableRenderer>();

		public void Sort(Vector3 sortPoint)
		{
			sortableRenderers.Clear();
			//List capacity does not change on Clear(), so using 'new' keyword with struct does not allocate new memory.
			foreach (var renderer in ActiveRenderers) sortableRenderers.Add(new SortableRenderer(renderer, sortPoint));

			if (sortOrder == SortOrder.Opaque) sortableRenderers.Sort(opaqueSortComparer);
			else if (sortOrder == SortOrder.Transparent) sortableRenderers.Sort(transparentSortComparer);

			for (int i = 0; i < sortableRenderers.Count; i++)
			{
				ActiveRenderers[i] = sortableRenderers[i].renderer;
			}
		}

		private Comparer<SortableRenderer> opaqueSortComparer = Comparer<SortableRenderer>.Create((a, b) =>
		{
			if (a.queue > b.queue) return 1;
			else if (a.queue < b.queue) return -1;
			else return (a.dist - b.dist < 0) ? -1 : 1;
		});

		private Comparer<SortableRenderer> transparentSortComparer = Comparer<SortableRenderer>.Create((a, b) =>
		{
			if (a.queue > b.queue) return 1;
			else if (a.queue < b.queue) return -1;
			else return (a.dist - b.dist < 0) ? 1 : -1;
		});

		public IEnumerator<VertexParticleInstance> GetEnumerator() => ActiveRenderers.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => ActiveRenderers.GetEnumerator();
	}
}
