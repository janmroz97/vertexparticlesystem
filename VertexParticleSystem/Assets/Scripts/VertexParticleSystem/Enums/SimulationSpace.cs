﻿using UnityEngine;
using UnityEditor;

namespace Assets.Scripts.VertexParticleSystem.Enums
{
    public enum SimulationSpace
    {
        Object = 0,
        World = 1
    }
}