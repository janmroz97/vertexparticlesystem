﻿namespace Assets.Scripts.VertexParticleSystem.Enums
{
    public enum PlayMode
    {
        OneShot, 
        Continuous, 
        Loop
    }

}