﻿using UnityEngine;
using UnityEditor;

namespace Assets.Scripts.VertexParticleSystem.Enums
{
    public enum PlayingState
    { 
        Stopped, 
        Paused, 
        Playing 
    }
}