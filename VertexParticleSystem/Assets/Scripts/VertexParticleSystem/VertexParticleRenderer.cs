﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.VertexParticleSystem
{
	public class VertexParticleRenderer : MonoBehaviour
	{
		VertexParticleInstance particleInstance = new VertexParticleInstance();

		[ContextMenu("Play")]
		public void Play() => particleInstance.Play();

		[ContextMenu("Stop")]
		public void Stop() => particleInstance.Stop();

		[ContextMenu("Pause")]
		public void Pause() => particleInstance.Pause();
	}
}
