﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.VertexParticleSystem
{
    public class PathRecorder
    {
        TransformReference transform;

        struct Entry
        {
            public float time;
            public Vector3 position;

            public Entry(float time, Vector3 position)
            {
                this.time = time;
                this.position = position;
            }
        }

        Queue<Entry> path = new Queue<Entry>();
        Queue<Entry> tempPath = new Queue<Entry>();
        Vector3[] pathArray = new Vector3[CommonShaderSettings.PATH_ARRAY_SIZE];
        float timer = 0.0f;
        float recordTime;

        public PathRecorder(TransformReference transform, float recordTime)
        {
            this.transform = transform;
            this.recordTime = recordTime;
            InitializePath();
        }

        public void Update(float deltaTime)
        {
            timer += deltaTime;
            UpdatePath();
        }

        public void UpdatePath()
        {
            while (HasAnyRegisteredPoint && IsLastPointOutdated)
            {
                path.Dequeue();
            }
            AddActualPositionToPath();
        }

        public Vector3[] GetPathArray()
        {
            CalculatePathArray();
            return pathArray;
        }

        public IEnumerable<Vector3> Path => path.Select(e => e.position);

        private void InitializePath()
        {
            path.Clear();
            AddActualPositionToPath();
            AddActualPositionToPath();
        }

        private void AddActualPositionToPath()
        {
            var actualPosition = new Entry(timer, transform.Position);
            path.Enqueue(actualPosition);
        }

        private bool HasAnyRegisteredPoint => path.Count > 0;
        private bool IsLastPointOutdated => (path.Count > 0) ? (path.Peek().time < timer - recordTime) : false;

        private void CalculatePathArray()
        {
            if (path.Count <= 1) 
            {
                InitializePath();
            }

            float deltaTime = recordTime / (pathArray.Length - 1);
            float pathPointTime = timer - recordTime;

            Entry pathEntry = path.Dequeue();
            tempPath.Enqueue(pathEntry);
            Entry nextPathEntry = path.Peek();

            for (int i = 0; i < pathArray.Length; i++)
            {
                while (nextPathEntry.time <= pathPointTime)
                {
                    if (path.Count == 0)
                    {
                        break;
                    }

                    pathEntry = path.Dequeue();
                    tempPath.Enqueue(pathEntry);

                    if (path.Count == 0)
                    {
                        break;
                    }
                    nextPathEntry = path.Peek();
                }

                if (Mathf.Approximately(pathEntry.time, nextPathEntry.time))
                {
                    pathArray[i] = Vector3.Lerp(pathEntry.position, nextPathEntry.position, 0.5f);
                }
                else
                {
                    float t = (pathPointTime - pathEntry.time) / (nextPathEntry.time - pathEntry.time);
                    t = Mathf.Clamp01(t);
                    pathArray[i] = Vector3.Lerp(pathEntry.position, nextPathEntry.position, t);
                }

                pathPointTime += deltaTime;
            }

            while (path.Count > 0)
            {
                tempPath.Enqueue(path.Dequeue());
            }

            SwapPathQueueWithTemp();
        }

        private void SwapPathQueueWithTemp()
        {
            path.Clear();

            while (tempPath.Count > 0)
            {
                path.Enqueue(tempPath.Dequeue());
            }
        }

        public void DealocateMemory()
        {
            path.Clear();
            path = null;
            tempPath.Clear();
            tempPath = null;
            pathArray = null;
            transform = null;
        }
    }
}