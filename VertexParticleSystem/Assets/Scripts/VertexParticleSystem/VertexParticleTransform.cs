﻿using UnityEngine;

namespace Assets.Scripts.VertexParticleSystem
{
	public struct VertexParticleTransform
	{
		public enum TransformMode { UnityTransform, Independent }

		public Transform transform;

		private Vector3 positionWS;
		public Vector3 PositionWS { get; set; }

		private Vector3 scaleWS;
		public Vector3 ScaleOS;

		private Quaternion rotationWS;
		public Quaternion 
	}
}
