﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.VertexParticleSystem
{
	[Serializable]
	public class VertexParticleInstance
	{
		public enum ParticlePlayingState { Stopped, Paused, Playing }
		public enum ParticlePlayMode { OneShot, Continuous, Loop }

		#region ParticleSettings

		[SerializeField]
		private int particlesCount = 1000;
		public int ParticlesCount
		{
			get => Mathf.Max(particlesCount, 1);
			set => particlesCount = Mathf.Max(value, 1);
		}

		[SerializeField]
		private VertexParticleLODSettings lodSettings;

		[SerializeField]
		private Material material = default;
		public Material Material => material;

		[SerializeField]
		private ParticlePlayMode playMode = ParticlePlayMode.OneShot;
		public ParticlePlayMode PlayMode => playMode;

		[SerializeField]
		private float duration = 5.0f;
		public float Duration
		{
			get => duration;
			set => duration = Mathf.Max(value, 0.0f);
		}

		[SerializeField]
		private TransformableBounds bounds = new TransformableBounds(Vector3.zero, Vector3.one);
		public Bounds Bounds => bounds.Transformed(transform.LocalToWorldMatrix);

		public VertexParticleTransform transform;

		#endregion

		#region RuntimeProperties

		public ParticlePlayingState State { get; private set; } = ParticlePlayingState.Stopped;
		public float Timer { get; private set; } = 0.0f;
		public float Seed { get; private set; } = 0.0f;

		//cant be null
		public Action<MaterialPropertyBlock> propertyBlockUpdater = (p) => { };

		#endregion

		private void Start()
		{
			Play();
		}

		//Called each frame by VertexParticleManager if renderer is registered as active
		internal void ParticleUpdate()
		{
			if (State == ParticlePlayingState.Playing) Timer += Time.deltaTime;

			switch (PlayMode)
			{
				case ParticlePlayMode.OneShot:
					if (Timer > duration) Stop();
					break;
				case ParticlePlayMode.Continuous:
					break;
				case ParticlePlayMode.Loop:
					while (Timer > duration)
					{
						Timer -= Duration;
						ResetSeed();
					}
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public void Play()
		{
			if (!IsValid)
			{
				Debug.LogWarning($"Invalid VertexParticleRenderer settings. Check assigned material (or other properties).");
				return;
			}

			switch (State)
			{
				case ParticlePlayingState.Playing:
					break;
				case ParticlePlayingState.Paused:
					State = ParticlePlayingState.Playing;
					break;
				case ParticlePlayingState.Stopped:
					VertexParticleManager.RegisterRenderer(this);
					State = ParticlePlayingState.Playing;
					ResetSeed();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public void Pause()
		{
			if (State == ParticlePlayingState.Playing) State = ParticlePlayingState.Paused;
		}

		public void Stop()
		{
			if (!IsValid)
			{
				Debug.LogWarning($"Invalid VertexParticleRenderer settings. Check assigned material (or other properties).");
				return;
			}

			switch (State)
			{
				case ParticlePlayingState.Playing:
				case ParticlePlayingState.Paused:
					VertexParticleManager.UnregisterRenderer(this);
					State = ParticlePlayingState.Stopped;
					Timer = 0.0f;
					break;
				case ParticlePlayingState.Stopped:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public void Restart()
		{
			SetPlaybackTime(0);
			ResetSeed();
		}

		public void ResetSeed() => Seed = Random.Range(0.0f, 10.0f);

		public void SetPlaybackTime(float time) => Timer = time;

		private bool IsValid => (material != null);

		public void GetPropertyBlock(MaterialPropertyBlock propertyBlock)
		{
			propertyBlockUpdater(propertyBlock);
		}

		public int LODAppliedParticleCount(Vector3 cameraPositionWS)
		{
			if (lodSettings == null) return particlesCount;
			else
			{
				float distToCamera = Vector3.Distance(transform.position, cameraPositionWS);
				return Mathf.Max((int)(ParticlesCount * lodSettings.EvaluateLODLevel(distToCamera)), 0);
			}
		}

		private void OnValidate()
		{
			duration = Mathf.Max(duration, 0.0f);
			particlesCount = Mathf.Max(particlesCount, 1);
		}

		private void OnDrawGizmos()
		{
			Gizmos.color = Color.cyan;
			Gizmos.DrawWireCube(Bounds.center, Bounds.size);
		}
	}
}
