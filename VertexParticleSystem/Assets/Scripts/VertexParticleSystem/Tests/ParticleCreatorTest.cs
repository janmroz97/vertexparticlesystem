﻿using UnityEngine;
using UnityEditor;

namespace Assets.Scripts.VertexParticleSystem.Tests
{
    public class ParticleCreatorTest : MonoBehaviour
    {
        [SerializeField]
        private LODSettings lodSettings = default;

        [SerializeField]
        private Material material = default;

        private new Renderer renderer = default;

        private void Start()
        {
            var particles = ParticleCreator.Begin()
                .Position(new Vector3(1.0f, 2.0f, 1.0f))
                .Rotation(Quaternion.Euler(1.0f, 42.0f, 63.0f))
                .Scale(new Vector3(3.0f, 3.0f, 3.0f))
                .Material(material)
                .ParticlesCount(100000)
                .LODSettings(lodSettings)
                .PlayMode(Enums.PlayMode.Continuous)
                .Duration(30.0f)
                .Bounds(new Bounds(Vector3.zero, Vector3.one * 4.0f))
                .Create();

            renderer = particles;
            particles.Play();
        }

        private void OnDrawGizmos()
        {
            renderer?.DrawGizmos();
        }

        [ContextMenu("Attach to object")]
        private void Attach()
        {
            renderer.transform.Attach(transform);
        }

        [ContextMenu("Detach from object")]
        private void Detach()
        {
            renderer.transform.Detach();
        }
    }
}