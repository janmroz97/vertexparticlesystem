﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace Assets.Scripts.VertexParticleSystem
{
	public class VertexParticleManager : MonoBehaviour
	{
		[SerializeField]
		private bool drawGUIStats = true;

		private static VertexParticleManager instance = default;
		public static bool Available { get; private set; } = false;

		private VertexParticleQueue beforeOpaqueQueue = new VertexParticleQueue(VertexParticleQueue.SortOrder.Opaque);
		private VertexParticleQueue afterOpaqueQueue = new VertexParticleQueue(VertexParticleQueue.SortOrder.Opaque);
		private VertexParticleQueue beforeTransparentQueue = new VertexParticleQueue(VertexParticleQueue.SortOrder.Transparent);
		private VertexParticleQueue afterTransparentQueue = new VertexParticleQueue(VertexParticleQueue.SortOrder.Transparent);

		public static VertexParticleQueue BeforeOpaqueQueue => instance.beforeOpaqueQueue;
		public static VertexParticleQueue AfterOpaqueQueue => instance.afterOpaqueQueue;
		public static VertexParticleQueue BeforeTransparentQueue => instance.beforeTransparentQueue;
		public static VertexParticleQueue AfterTransparentQueue => instance.afterTransparentQueue;

		private void OnEnable()
		{
			if (instance != null) Destroy(this);
			else
			{
				instance = this;
				VertexParticleMesh.Initialize();
				Available = true;
			}
		}

		private void Update()
		{
			UpdateQueueRenderers(beforeOpaqueQueue);
			UpdateQueueRenderers(afterOpaqueQueue);
			UpdateQueueRenderers(beforeTransparentQueue);
			UpdateQueueRenderers(afterTransparentQueue);
		}

		float fps = 0;
		private void OnGUI()
		{
			if (drawGUIStats)
			{
				fps = Mathf.Lerp(fps, 1.0f / Time.deltaTime, 0.2f);
				GUI.TextField(new Rect(0, 0, 200, 60), $"Particles: {VertexParticleFeature.ParticlesCountStats}\nFPS: {(int)fps}\nDraw calls: {VertexParticleFeature.DrawCallsStats}");
			}
		}

		private void OnDisable()
		{
			Available = false;
		}

		public static void RegisterRenderer(VertexParticleInstance particleInstance)
		{
			instance.QueueFromMaterial(particleInstance.Material).EnqueueAddCommand(particleInstance);
		}

		public static void UnregisterRenderer(VertexParticleInstance particleInstance)
		{
			instance.QueueFromMaterial(particleInstance.Material).EnqueueRemoveCommand(particleInstance);
		}

		public const float SeedDifference = 0.237851752f;

		private VertexParticleQueue QueueFromMaterial(Material material)
		{
			int queue = material.renderQueue;
			if (queue <= 2000) return beforeOpaqueQueue;
			else if (queue <= 2500) return afterOpaqueQueue;
			else if (queue <= 3000) return beforeTransparentQueue;
			else return afterTransparentQueue;
		}

		private void UpdateQueueRenderers(VertexParticleQueue queue)
		{
			queue.ExecuteCommands();
			for (int i = 0; i < queue.ActiveRenderers.Count; i++) queue.ActiveRenderers[i]?.ParticleUpdate();
		}
	}
}
