﻿using UnityEngine;

namespace Assets.Scripts.VertexParticleSystem
{
	internal static class Utils
	{
		public static void Corners(this Bounds b, ref Vector4[] corners)
		{
			Vector3 min = b.min, max = b.max;
			corners[0] = new Vector4(min.x, min.y, min.z, 1.0f);
			corners[1] = new Vector4(min.x, min.y, max.z, 1.0f);
			corners[2] = new Vector4(min.x, max.y, min.z, 1.0f);
			corners[3] = new Vector4(min.x, max.y, max.z, 1.0f);
			corners[4] = new Vector4(max.x, min.y, min.z, 1.0f);
			corners[5] = new Vector4(max.x, min.y, max.z, 1.0f);
			corners[6] = new Vector4(max.x, max.y, min.z, 1.0f);
			corners[7] = new Vector4(max.x, max.y, max.z, 1.0f);
		}
	}
}
