﻿using UnityEngine;
using UnityEditor;
using System.Runtime.CompilerServices;

namespace Assets.Scripts.VertexParticleSystem
{
    public class ParticleCreator
    {
        private Renderer renderer = new Renderer();

        public ParticleCreator Position(Vector3 position)
        {
            renderer.transform.Position = position;
            return this;
        }

        public ParticleCreator Scale(Vector3 scale)
        {
            renderer.transform.Scale = scale;
            return this;
        }

        public ParticleCreator Rotation(Quaternion rotation)
        {
            renderer.transform.Rotation = rotation;
            return this;
        }

        public ParticleCreator LinkedTransform(Transform transform)
        {
            renderer.transform.Attach(transform);
            return this;
        }

        public ParticleCreator ParticlesCount(int count)
        {
            renderer.ParticlesCount = count;
            return this;
        }

        public ParticleCreator Material(Material material)
        {
            renderer.Material = material;
            return this;
        }

        public ParticleCreator Duration(float duration)
        {
            renderer.Duration = duration;
            return this;
        }

        public ParticleCreator Bounds(Bounds bounds)
        {
            renderer.Bounds = bounds;
            return this;
        }

        public ParticleCreator LODSettings(LODSettings lodSettings)
        {
            renderer.LODSettings = lodSettings;
            return this;
        }

        public ParticleCreator PlayMode(Enums.PlayMode playMode)
        {
            renderer.PlayMode = playMode;
            return this;
        }


        public static ParticleCreator Begin() => new ParticleCreator();

        public Renderer Create() => renderer;
    }
}