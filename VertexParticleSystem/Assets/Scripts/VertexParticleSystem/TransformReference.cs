﻿using UnityEngine;
using UnityEditor;
using System;

namespace Assets.Scripts.VertexParticleSystem
{
    [Serializable]
    public class TransformReference
    {
        private Transform transform;
        private Vector3 position;
        private Vector3 rotation;
        private Vector3 scale;

        public TransformReference(Transform transform)
        {
            this.transform = transform;
        }

        public TransformReference(Vector3 position, Vector3 rotation, Vector3 scale)
        {
            this.position = position;
            this.rotation = rotation;
            this.scale = scale;
        }

        public TransformReference() { }

        public Matrix4x4 LocalToWorldMatrix => HasLinkedTransform ? transform.localToWorldMatrix : Matrix4x4.TRS(position, Quaternion.Euler(rotation), scale);
        public bool HasLinkedTransform => transform != null;

        public Vector3 Position
        {
            get => transform?.position ?? position;
            set
            {
                if (HasLinkedTransform) transform.position = value;
                else position = value;
            }
        }
        public Quaternion Rotation
        {
            get => transform?.rotation ?? Quaternion.Euler(rotation);
            set
            {
                if (HasLinkedTransform) transform.rotation = value;
                else position = value.eulerAngles;
            }
        }

        public Vector3 Scale
        {
            get => transform?.localScale ?? scale;
            set
            {
                if (HasLinkedTransform) transform.localScale = value;
                else scale = value;
            }
        }

        public Vector3 LossyScale => transform?.lossyScale ?? scale;

        public Transform Transform => transform;

        public void Attach(Transform transform) => this.transform = transform;

        public void Detach()
        {
            if (HasLinkedTransform)
            {
                position = transform.position;
                rotation = transform.rotation.eulerAngles;
                scale = transform.localScale;
                transform = null;
            }
        }
    }
}