﻿using UnityEngine;
using UnityEditor;

namespace Assets.Scripts.VertexParticleSystem
{
    public static class CommonShaderSettings
    {
        public const int PATH_ARRAY_SIZE = 256;
    }
}