﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace Assets.Scripts.VertexParticleSystem
{
	class VertexParticleFeature : ScriptableRendererFeature
	{
		public static int DrawCallsStats { get; set; }
		public static int ParticlesCountStats { get; set; }

		private VertexParticleRenderPass beforeOpaquePass;
		private VertexParticleRenderPass afterOpaquePass;
		private VertexParticleRenderPass beforeTransparentPass;
		private VertexParticleRenderPass afterTransparentPass;

		public static Material shaderNotSupportedMaterial = default;

		public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
		{
			DrawCallsStats = 0;
			ParticlesCountStats = 0;

			if (VertexParticleManager.Available)
			{
				if (VertexParticleManager.BeforeOpaqueQueue.HasRenderers)
				{
					beforeOpaquePass.Setup(RenderPassEvent.BeforeRenderingOpaques, VertexParticleManager.BeforeOpaqueQueue);
					renderer.EnqueuePass(beforeOpaquePass);
				}

				if (VertexParticleManager.AfterOpaqueQueue.HasRenderers)
				{
					afterOpaquePass.Setup(RenderPassEvent.AfterRenderingOpaques, VertexParticleManager.AfterOpaqueQueue);
					renderer.EnqueuePass(afterOpaquePass);
				}

				if (VertexParticleManager.BeforeTransparentQueue.HasRenderers)
				{
					beforeTransparentPass.Setup(RenderPassEvent.BeforeRenderingTransparents, VertexParticleManager.BeforeTransparentQueue);
					renderer.EnqueuePass(beforeTransparentPass);
				}

				if (VertexParticleManager.AfterTransparentQueue.HasRenderers)
				{
					afterTransparentPass.Setup(RenderPassEvent.AfterRenderingTransparents, VertexParticleManager.AfterTransparentQueue);
					renderer.EnqueuePass(afterTransparentPass);
				}
			}
		}

		public override void Create()
		{
			shaderNotSupportedMaterial = new Material(Shader.Find("Hidden/Vertex Particle System/ErrorFallback"));
			beforeOpaquePass = new VertexParticleRenderPass("VertexParticle before opaque");
			afterOpaquePass = new VertexParticleRenderPass("VertexParticle after opaque");
			beforeTransparentPass = new VertexParticleRenderPass("VertexParticle before transparents");
			afterTransparentPass = new VertexParticleRenderPass("VertexParticle after transparents");
		}

		private class VertexParticleRenderPass : ScriptableRenderPass
		{
			CommandBuffer commandBuffer;
			private int shaderPropertyID_Seed;
			private int shaderPropertyID_ParticlesCount;
			private int shaderPropertyID_StartParticleIndex;
			private int shaderPropertyID_ParticleTime;

			private VertexParticleQueue renderersQueue;

			public void Setup(RenderPassEvent renderPassEvent, VertexParticleQueue renderersQueue)
			{
				this.renderPassEvent = renderPassEvent;
				this.renderersQueue = renderersQueue;
			}

			public VertexParticleRenderPass(string passName)
			{
				renderPassEvent = RenderPassEvent.BeforeRenderingTransparents;
				shaderPropertyID_Seed = Shader.PropertyToID("_Seed");
				shaderPropertyID_ParticlesCount = Shader.PropertyToID("_ParticlesCount");
				shaderPropertyID_StartParticleIndex = Shader.PropertyToID("_StartParticleIndex");
				shaderPropertyID_ParticleTime = Shader.PropertyToID("_ParticleTime");
				commandBuffer = new CommandBuffer
				{
					name = passName
				};
			}

			#region PropertiesForPropertyBlock

			List<VertexParticleMeshData> meshesToRender = new List<VertexParticleMeshData>(50);
			float seed = 0.0f;
			Matrix4x4 objectToWorldMatrix;
			MaterialPropertyBlock propertyBlock = new MaterialPropertyBlock();
			Material material;
			float time;

			#endregion

			Plane[] frustrumPlanes = new Plane[6];

			public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
			{
				commandBuffer.Clear();

				GeometryUtility.CalculateFrustumPlanes(renderingData.cameraData.camera, frustrumPlanes);

				//sorting by distance to camera
				renderersQueue.Sort(renderingData.cameraData.camera.transform.position);

				//cached for faster LOD calculation
				Vector3 cameraPositionWS = renderingData.cameraData.camera.transform.position;
				int particleCount = 0;

				for (int rendererIndex = 0; rendererIndex < renderersQueue.ActiveRenderers.Count; rendererIndex++)
				{
					var particleInstance = renderersQueue.ActiveRenderers[rendererIndex];

					if (GeometryUtility.TestPlanesAABB(frustrumPlanes, particleInstance.Bounds))
					{
						//TODO check if shader was compiled properly

						particleCount = particleInstance.LODAppliedParticleCount(cameraPositionWS);

						propertyBlock.Clear();
						objectToWorldMatrix = particleInstance.LocalToWorldMatrix;
						VertexParticleMesh.GetMeshes(particleCount, ref meshesToRender);
						particleInstance.GetPropertyBlock(propertyBlock);
						seed = particleInstance.Seed;
						material = particleInstance.Material;
						time = particleInstance.Timer;
						ParticlesCountStats += particleCount;

						//per renderer properties
						propertyBlock.SetFloat(shaderPropertyID_Seed, seed);
						propertyBlock.SetInt(shaderPropertyID_ParticlesCount, particleInstance.ParticlesCount);
						propertyBlock.SetFloat(shaderPropertyID_ParticleTime, time);

						foreach (var meshData in meshesToRender)
						{
							//stats calculation
							DrawCallsStats++;
							
							//per draw call properties
							propertyBlock.SetInt(shaderPropertyID_StartParticleIndex, meshData.startParticleIndex);
							
							commandBuffer.DrawMesh(meshData.mesh, objectToWorldMatrix, material, 0, 0, propertyBlock);
							
							seed += VertexParticleManager.SeedDifference;
						}
					}
				}

				context.ExecuteCommandBuffer(commandBuffer);
			}
		}
	}
}
