﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.VertexParticleSystem
{
	public struct PartialMesh
	{
		public UnityEngine.Mesh mesh;
		public int startParticleIndex;

		public PartialMesh(UnityEngine.Mesh mesh, int startParticleIndex)
		{
			this.mesh = mesh;
			this.startParticleIndex = startParticleIndex;
		}
	}
}
