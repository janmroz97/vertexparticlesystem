﻿using System;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Rendering;

namespace Assets.Scripts.VertexParticleSystem
{
	/// <summary>
	/// Used for getting correct meshes (compatible with particle shader) to draw particular amount of particles.
	/// </summary>
	public static class Mesh
	{
		/// <summary>
		/// Max particle count in single draw call. 
		/// </summary>
		private const int MaxParticleCount = 16000;

		//--------------------------------------- MESHES METADATA -------------------------------------------

		/// <summary>
		/// Description of single vertex data. (What VertexShader will receive)
		/// </summary>
		private static VertexAttributeDescriptor[] particleVertexDescriptor = new[]
		{
			new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float32, 4, 0),
			new VertexAttributeDescriptor(VertexAttribute.TexCoord0, VertexAttributeFormat.UInt32, 1, 0)
		};

		/// <summary>
		/// Vertex data representant in C#. It is converted to vertex data and sent to GPU by Unity Engine.
		/// </summary>
		[System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
		private struct ParticleVertex
		{
			public Vector4 positionOS;
			public UInt32 id;
		}

		private static ParticleVertex[] fullParticleVertices;
		private static ushort[] fullParticleTriangleIndices;

		private static Vector4[] singleParticleVertices = new Vector4[]
		{
			new Vector4(0.5f, 0.5f, 0.0f, 1.0f),
			new Vector4(-0.5f, 0.5f, 0.0f, 1.0f),
			new Vector4(-0.5f, -0.5f, 0.0f, 1.0f),
			new Vector4(0.5f, -0.5f, 0.0f, 1.0f)
		};

		private static ParticleVertex[] testMeshVerticesData = new ParticleVertex[]
		{
			new ParticleVertex() { positionOS = new Vector4(0.5f, 0.5f, 0.0f, 1.0f), id = 0 },
			new ParticleVertex() { positionOS = new Vector4(-0.5f, 0.5f, 0.0f, 1.0f), id = 0 },
			new ParticleVertex() { positionOS = new Vector4(-0.5f, -0.5f, 0.0f, 1.0f), id = 0 },
			new ParticleVertex() { positionOS = new Vector4(0.5f, -0.5f, 0.0f, 1.0f), id = 0 }
		};

		//indices are UInt16 instead of UInt32 to make the mesh much smaller
		private static ushort[] singleParticleIndices = new ushort[] { 0, 2, 1, 3, 2, 0 };

		//--------------------------------------- BAKED DATA -------------------------------------------------------

		private static Dictionary<int, UnityEngine.Mesh> bakedMeshes = new Dictionary<int, UnityEngine.Mesh>();

		/// <summary>
		/// Gets all meshes needed to draw particular number of particles.
		/// <para>Ex. 19853 particles will be rendered using 3 draw calls with: 10000, 9000 and 853 particles.</para>
		/// </summary>
		/// <param name="particleCount">Particles to render</param>
		/// <param name="meshes">List of meshes to set</param>
		public static void GetMeshes(int particleCount, ref List<PartialMesh> meshes)
		{
			meshes.Clear();
			int firstMeshParticleIndex = 0;
			while (particleCount >= MaxParticleCount)
			{
				meshes.Add(new PartialMesh(bakedMeshes[MaxParticleCount], firstMeshParticleIndex));
				particleCount -= MaxParticleCount;
				firstMeshParticleIndex += MaxParticleCount;
			}

			int count1000 = (particleCount / 1000) * 1000;
			if (count1000 > 0)
			{
				meshes.Add(new PartialMesh(bakedMeshes[count1000], firstMeshParticleIndex));
				particleCount -= count1000;
				firstMeshParticleIndex += count1000;
			}

			int count100 = (particleCount / 100) * 100;
			if (count100 > 0)
			{
				meshes.Add(new PartialMesh(bakedMeshes[count100], firstMeshParticleIndex));
				particleCount -= count100;
				firstMeshParticleIndex += count100;
			}


			if (particleCount > 0) meshes.Add(new PartialMesh(bakedMeshes[particleCount], firstMeshParticleIndex));
		}

		/// <summary>
		/// Gets all meshes needed to draw particular number of particles
		/// </summary>
		/// <param name="particleCount">Particles to render</param>
		public static List<PartialMesh> GetMeshes(int particleCount)
		{
			var meshDataList = new List<PartialMesh>();
			GetMeshes(particleCount, ref meshDataList);
			return meshDataList;
		}

		//--------------------------------------- INITIALIZATION ---------------------------------------------------

		/// <summary>
		/// Initializes all needed data, and bakes all meshes.
		/// </summary>
		public static void Initialize()
		{
			fullParticleTriangleIndices = new ushort[MaxParticleCount * 6];
			fullParticleVertices = new ParticleVertex[MaxParticleCount * 4];

			CalculateFullData();
			BakeMeshes();
			var lul = TestMesh;
		}

		//------------------------------- BAKING PARTICLE MESHES ------------------------------------------------------

		/// <summary>
		/// Fills <see cref="fullParticleVertices"/> and <see cref="fullParticleTriangleIndices"/> 
		/// </summary>
		private static void CalculateFullData()
		{
			//vertex data
			UInt32 particleID = 0;
			for (int i = 0; i < fullParticleVertices.Length; i += 4)
			{
				fullParticleVertices[i + 0] = new ParticleVertex() { positionOS = singleParticleVertices[0], id = particleID };
				fullParticleVertices[i + 1] = new ParticleVertex() { positionOS = singleParticleVertices[1], id = particleID };
				fullParticleVertices[i + 2] = new ParticleVertex() { positionOS = singleParticleVertices[2], id = particleID };
				fullParticleVertices[i + 3] = new ParticleVertex() { positionOS = singleParticleVertices[3], id = particleID };
				particleID++;
			}

			//triangle indices
			ushort baseVertexIndex = 0;
			for (int i = 0; i < fullParticleTriangleIndices.Length; i += 6)
			{
				fullParticleTriangleIndices[i + 0] = (ushort)(baseVertexIndex + singleParticleIndices[0]);
				fullParticleTriangleIndices[i + 1] = (ushort)(baseVertexIndex + singleParticleIndices[1]);
				fullParticleTriangleIndices[i + 2] = (ushort)(baseVertexIndex + singleParticleIndices[2]);

				fullParticleTriangleIndices[i + 3] = (ushort)(baseVertexIndex + singleParticleIndices[3]);
				fullParticleTriangleIndices[i + 4] = (ushort)(baseVertexIndex + singleParticleIndices[4]);
				fullParticleTriangleIndices[i + 5] = (ushort)(baseVertexIndex + singleParticleIndices[5]);

				baseVertexIndex += 4;
			}
		}

		/// <summary>
		/// Fills <see cref="bakedMeshes"/> dictionary with baked particle meshes  
		/// Small amount of meshes are baked, to reduce memory allocation.  
		/// Concrete amount of particles is rendered by rendering couple of meshes.  
		/// Ex. 19853 particles will be rendered using 3 draw calls with: 16000, 3000, 800 and 53 particles.
		/// <para>This Method works strictly with <see cref="GetMeshes(int, ref List{PartialMesh})"/></para>
		/// </summary>
		private static void BakeMeshes()
		{
			//Range 1-100, step 1
			for (int i = 1; i <= 100; i++) BakeMesh(i);

			//Range 100-1000 step 1000
			for (int i = 200; i <= 1000; i += 100) BakeMesh(i);

			//Range 1000-MaxParticleCount step 1000
			for (int i = 2000; i <= MaxParticleCount; i += 1000) BakeMesh(i);
		}

		/// <summary>
		/// Baking single mesh and placing it in <see cref="bakedMeshes{int,Mesh}"/> dictionary;
		/// </summary>
		/// <param name="particlesCount">Number of particles to bake</param>
		private static void BakeMesh(int particlesCount)
		{
			var mesh = new UnityEngine.Mesh();

			mesh.SetVertexBufferParams(particlesCount * 4, particleVertexDescriptor);
			mesh.SetIndexBufferParams(particlesCount * 6, IndexFormat.UInt16);
			mesh.SetVertexBufferData(fullParticleVertices, 0, 0, particlesCount * 4);
			mesh.SetIndexBufferData(fullParticleTriangleIndices, 0, 0, particlesCount * 6);
			mesh.bounds = new Bounds(Vector3.zero, Vector3.one * 1000.0f);
			mesh.subMeshCount = 1;
			mesh.SetSubMesh(0, new SubMeshDescriptor(0, particlesCount * 6, MeshTopology.Triangles));

			bakedMeshes.Add(particlesCount, mesh);
		}

		private static UnityEngine.Mesh testMesh = null;


		/// <summary>
		/// Mesh containing single particle with ID = 0.
		/// Used to test or display error fallback shaders.
		/// </summary>
		public static UnityEngine.Mesh TestMesh
		{
			get
			{
				if (testMesh != null) return testMesh;
				else
				{
                    testMesh = new UnityEngine.Mesh();

					testMesh.SetVertexBufferParams(4, particleVertexDescriptor);
					testMesh.SetVertexBufferData(testMeshVerticesData, 0, 0, 4);
					testMesh.SetIndexBufferParams(6, IndexFormat.UInt16);
					testMesh.SetIndexBufferData(singleParticleIndices, 0, 0, 6);
					testMesh.bounds = new Bounds(Vector3.zero, Vector3.one * 3.0f);
					testMesh.subMeshCount = 1;
					testMesh.SetSubMesh(0, new SubMeshDescriptor(0, 6, MeshTopology.Triangles));

					return testMesh;
				}
			}
		}
	}
}
