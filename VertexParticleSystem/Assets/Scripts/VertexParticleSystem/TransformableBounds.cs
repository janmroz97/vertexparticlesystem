﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.VertexParticleSystem
{
	//encapsulates Bounds class just to reduce GC allocations
	[Serializable]
	public class TransformableBounds
	{
		private Vector4[] corners = new Vector4[8];
		public Bounds bounds = new Bounds();

		public static implicit operator Bounds(TransformableBounds b) => b.bounds;

		public TransformableBounds() { }

		public TransformableBounds(Vector3 center, Vector3 size)
		{
			bounds = new Bounds(center, size);
		}
		
		public Bounds Transformed(Matrix4x4 matrix)
		{
			bounds.Corners(ref corners);
			Vector4 min = matrix * corners[0];
			Vector4 max = matrix * corners[0];
			Vector4 temp;
			for (int i = 1; i < 8; i++)
			{
				temp = matrix * corners[i];
				min.x = Mathf.Min(min.x, temp.x);
				min.y = Mathf.Min(min.y, temp.y);
				min.z = Mathf.Min(min.z, temp.z);

				max.x = Mathf.Max(max.x, temp.x);
				max.y = Mathf.Max(max.y, temp.y);
				max.z = Mathf.Max(max.z, temp.z);
			}

			var newBounds = new Bounds();
			newBounds.SetMinMax(min, max);
			return newBounds;
		}
		
		public void Transform(Matrix4x4 matrix)
		{
			bounds.Corners(ref corners);
			Vector4 min = matrix * corners[0];
			Vector4 max = matrix * corners[0];
			Vector4 temp;
			for (int i = 1; i < 8; i++)
			{
				temp = matrix * corners[i];
				min.x = Mathf.Min(min.x, temp.x);
				min.y = Mathf.Min(min.y, temp.y);
				min.z = Mathf.Min(min.z, temp.z);

				max.x = Mathf.Max(max.x, temp.x);
				max.y = Mathf.Max(max.y, temp.y);
				max.z = Mathf.Max(max.z, temp.z);
			}
			
			bounds.SetMinMax(min, max);
		}
	}
}
