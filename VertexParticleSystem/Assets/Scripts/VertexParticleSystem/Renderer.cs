﻿using Assets.Scripts.VertexParticleSystem.Enums;
using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.VertexParticleSystem
{
	[Serializable]
	public class Renderer
	{
		[HideInInspector]
		public TransformReference transform = new TransformReference();

		#region InspectorProperties

		[SerializeField]
		private int particlesCount = 1000;
		public int ParticlesCount
		{
			get => Mathf.Max(particlesCount, 1);
			set => particlesCount = Mathf.Max(value, 1);
		}

		[SerializeField]
		private LODSettings lodSettings = default;
		public LODSettings LODSettings
		{
			get => lodSettings;
			set => lodSettings = value;
		}

		[SerializeField]
		private Material material = default;
		public Material Material
		{
			get => material;
			set => material = value;
		}

		[SerializeField]
		private Enums.PlayMode playMode = Enums.PlayMode.OneShot;
		public Enums.PlayMode PlayMode
		{
			get => playMode;
			set => playMode = value;
		}

		[SerializeField]
		private float duration = 5.0f;
		public float Duration
		{
			get => duration;
			set => duration = Mathf.Max(value, 0.0f);
		}

		[SerializeField]
		private SimulationSpace simulationSpace = SimulationSpace.Object;


		[SerializeField]
		private TransformableBounds transformableBounds = new TransformableBounds(Vector3.zero, Vector3.one);
		public Bounds TransformedBounds => transformableBounds.Transformed(transform.LocalToWorldMatrix);

		public Bounds Bounds
        {
			get => transformableBounds.bounds;
			set => transformableBounds.bounds = value;
        }

		#endregion


		#region RuntimeProperties

		public PlayingState State { get; private set; } = PlayingState.Stopped;
		public float Timer { get; private set; } = 0.0f;
		public float Seed { get; private set; } = 0.0f;

		//cant be null
		public Action<MaterialPropertyBlock> propertyBlockUpdater = (p) => {};

		private PathRecorder pathRecorder = null;

		#endregion

		//Called each frame by VertexParticleManager if renderer is registered as active
		internal void ParticleUpdate()
		{
			if (State == PlayingState.Playing) 
            {
				Timer += Time.deltaTime;
				if (simulationSpace == SimulationSpace.World)
                {
					pathRecorder.Update(Time.deltaTime);
                }
			}

			switch (PlayMode)
			{
				case Enums.PlayMode.OneShot:
					if (Timer > duration) Stop();
					break;
				case Enums.PlayMode.Continuous:
					break;
				case Enums.PlayMode.Loop:
					while (Timer > duration) 
					{
                        Timer -= Duration;
                        ResetSeed();
					}
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		[ContextMenu("Play")]
		public void Play()
		{
			if (!IsValid)
			{
				Debug.LogWarning($"Invalid VertexParticleRenderer settings. Check assigned material (or other properties).");
				return;
			}

			switch (State)
			{
				case PlayingState.Playing:
					break;
				case PlayingState.Paused:
					State = PlayingState.Playing;
					ManagePathRecorder();
					break;
				case PlayingState.Stopped:
					VertexParticleManager.RegisterRenderer(this);
					State = PlayingState.Playing;
					ResetSeed();
					ManagePathRecorder();
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		[ContextMenu("Pause")]
		public void Pause()
		{
			if (State == PlayingState.Playing) State = PlayingState.Paused;
		}

		[ContextMenu("Stop")]
		public void Stop()
		{
			if (!IsValid)
			{
				Debug.LogWarning($"Invalid VertexParticleRenderer settings. Check assigned material (or other properties).");
				return;
			}

			switch (State)
			{
				case PlayingState.Playing:
				case PlayingState.Paused:
					VertexParticleManager.UnregisterRenderer(this);
					State = PlayingState.Stopped;
					Timer = 0.0f;
					break;
				case PlayingState.Stopped:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		[ContextMenu("Restart")]
		public void Restart()
		{
			SetPlaybackTime(0);
			ResetSeed();
		}

		public void ResetSeed() => Seed = Random.Range(0.0f, 10.0f);

		public void SetPlaybackTime(float time) => Timer = time;

		private bool IsValid => (material != null);

		public void GetPropertyBlock(MaterialPropertyBlock propertyBlock)
		{
			propertyBlockUpdater(propertyBlock);
		}

		public int LODAppliedParticleCount(Vector3 cameraPositionWS)
		{
			if (lodSettings == null) return particlesCount;
			else
			{
				float distToCamera = Vector3.Distance(transform.Position, cameraPositionWS);
				return Mathf.Max((int)(ParticlesCount * lodSettings.EvaluateLODLevel(distToCamera)), 0);
			}
		}

		public void Validate()
		{
			duration = Mathf.Max(duration, 0.0f);
			particlesCount = Mathf.Max(particlesCount, 1);
			ManagePathRecorder();
		}

		public void DrawGizmos()
		{
			Gizmos.color = Color.cyan;
			Gizmos.DrawWireCube(TransformedBounds.center, TransformedBounds.size);

			if (pathRecorder != null)
            {
				var path = pathRecorder.GetPathArray();
				Gizmos.color = Color.green;
				foreach (var point in path)
                {
					Gizmos.DrawWireSphere(point, 0.03f);
                }

				var pathRaw = pathRecorder.Path;
				Gizmos.color = Color.red * new Color(1.0f, 1.0f, 1.0f, 0.2f);
				foreach (var point in pathRaw)
                {
					Gizmos.DrawWireSphere(point, 0.01f);
                }
            }
		}

        public override string ToString()
        {
            if (transform.HasLinkedTransform)
				return $"{transform.Transform.gameObject.name}. Particles count: {particlesCount}, material: {material}";
			else
				return $"No component renderer. Particles count: {particlesCount}, material: {material}";
        }

		public void SetSimulationSpace(SimulationSpace space)
        {
			if (space == simulationSpace) return;
			else
            {
				ManagePathRecorder();
            }
        }

		private void ManagePathRecorder()
        {
			if (simulationSpace == SimulationSpace.Object && pathRecorder != null)
            {
				pathRecorder.DealocateMemory();
				pathRecorder = null;
            }
			else if (simulationSpace == SimulationSpace.World && pathRecorder == null)
            {
				pathRecorder = new PathRecorder(transform, Duration);
            }
        }
    }
}
