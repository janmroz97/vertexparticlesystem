﻿using UnityEngine;

namespace Assets.Scripts.VertexParticleSystem
{
	[CreateAssetMenu(fileName = "LOD Settings", menuName = "Vertex Particle System/LOD Settings")]
	public class LODSettings : ScriptableObject
	{
		[SerializeField]
		private float minLodDistance = 4.0f;
		public float MinLodDistance => minLodDistance;

		[SerializeField]
		private float maxLodDistance = 100.0f;
		public float MaxLodDistance => maxLodDistance;

		[SerializeField]
		private AnimationCurve lodCurve = new AnimationCurve(new Keyframe(0.0f, 1.0f), new Keyframe(1.0f, 0.0f));

		[SerializeField]
		private bool stepMode = false;
		public bool StepMode => stepMode;

		[SerializeField]
		private int stepsCount = 10;
		public int StepsCount => StepsCount;

		//From 0 to 1 (min to max visibility)
		public float EvaluateLODLevel(float distance)
		{
			float t = Mathf.Clamp01((distance - minLodDistance) / (maxLodDistance - minLodDistance));
			if (stepMode) t = Mathf.RoundToInt(t * stepsCount) / (float)stepsCount;
			return lodCurve.Evaluate(t);
		}

		private void OnValidate()
		{
			stepsCount = Mathf.Max(stepsCount, 1);
			minLodDistance = Mathf.Max(minLodDistance, 0.0f);
			maxLodDistance = Mathf.Max(maxLodDistance, minLodDistance);
		}
	}
}
